package com.fressle.graph;

import java.util.*;
import junit.framework.TestCase;

/**
 *
 * @author rstjohn
 */
public class GraphMathTest extends TestCase {

    private Graph graph;
    private Node n0, n1, n2, n3, n4, n5, n6, n7;

    public GraphMathTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        graph = new Graph();

        n0 = new Node("0");
        n1 = new Node("1");
        n2 = new Node("2");
        n3 = new Node("3");
        n4 = new Node("4");
        n5 = new Node("5");
        n6 = new Node("6");
        n7 = new Node("7");

        graph.addEdge(n0, n1);
        graph.addEdge(n0, n3);
//        graph.addEdge(n2, n1);
        graph.addEdge(n1, n2);
        graph.addEdge(n1, n3);
        graph.addEdge(n0, new Node("17"));
        graph.addEdge(n2, new Node("15"));
        graph.addEdge(n2, new Node("16"));


        graph.addEdge(n7, n5);
        graph.addEdge(n6, n7);
        graph.addEdge(n5, n4);
        graph.addEdge(n5, n6);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testBFS() {
        List<Set<Node>> cc = new ArrayList();
        Set<Node> nodes = graph.getSourceNodeSet();
        Set<Node> components;
        for (Node node : nodes) {
            if (node.visited) continue;
            components = GraphMath.breadthFirstSearch(graph, node);
            cc.add(components);
        }
        System.out.println(cc);
        assertTrue(true);
    }
//    
//    public void testConnectedComponents() {
//        Set<Node> nodes = graph.getSourceNodeSet();
//        List<Set<Node>> cc = new ArrayList();
//        Set<Node> components;
//        for (Node node : nodes) {
//            if (node.visited) continue;
//
//            components = GraphMath.connectedComponent(graph, node);
//            cc.add(components);
//        }
//        System.out.println(cc);
//    }
//    /**
//     * Test of tarjan method, of class GraphMath.
//     */
//    public void testTarjan() {
//        System.out.println("tarjan");
//        AdjacencyList graph = null;
//        ArrayList expResult = null;
//        ArrayList result = GraphMath.tarjan(graph);
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of kosaraju method, of class GraphMath.
//     */
//    public void testKosaraju() {
//        System.out.println("kosaraju");
//        Node node = null;
//        AdjacencyList graph = null;
//        ArrayList expResult = null;
//        ArrayList result = GraphMath.kosaraju(node, graph);
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of kruskal method, of class GraphMath.
//     */
//    public void testKruskal() {
//        System.out.println("kruskal");
//        ArrayList<Edge> edges = null;
//        Node[] nodes = null;
//        ArrayList expResult = null;
//        ArrayList result = GraphMath.kruskal(edges, nodes);
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of dijkstra method, of class GraphMath.
//     */
//    public void testDijkstra() {
//        System.out.println("dijkstra");
//        Node[] nodes = null;
//        Edge[] edges = null;
//        Node target = null;
//        ArrayList expResult = null;
//        ArrayList result = GraphMath.dijkstra(nodes, edges, target);
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
}
