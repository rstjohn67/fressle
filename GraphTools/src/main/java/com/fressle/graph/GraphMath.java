package com.fressle.graph;

import java.util.*;

/**
 *
 * @author rstjohn
 */
public class GraphMath {

    /**
     * Tarjan's algorithm is a variation (slightly faster) on Kosaraju's
     * algorithm for finding strongly-connected components in a directed graph.
     *
     * @author rstjohn
     */
    public static ArrayList<ArrayList<Node>> tarjan(Graph graph) {
        return new Tarjan().executeTarjan(graph);
    }

    /**
     * Kosaraju's algorithm finds strongly-connected components in a directed
     * graph. Tarjan's algorithm is a slightly faster variation on this.
     *
     * @author rstjohn
     */
    public static ArrayList<ArrayList<Node>> kosaraju(Node node, Graph graph) {
        return new Kosaraju().getSCC(node, graph);
    }

    /**
     * Kruskal's algorithm finds a minimum spanning tree for a connected,
     * weighted, undirected graph.
     *
     * @author rstjohn
     */
    public static ArrayList<Edge> kruskal(ArrayList<Edge> edges, Node... nodes) {
        Collections.sort(edges);
        ArrayList<Edge> MST = new ArrayList<Edge>();
        DisjointSet<Node> nodeset = new DisjointSet<Node>();
        nodeset.createSubsets(nodes);
        for (Edge e : edges) {
            if (nodeset.find(e.from) != nodeset.find(e.to)) {
                nodeset.merge(nodeset.find(e.from), nodeset.find(e.to));
                MST.add(e);
            }
        }
        return MST; //wors
    }

    public static Set<Node> breadthFirstSearch(Graph graph, Node node) {
        Set<Node> nodes = new HashSet<Node>();
        
        Queue<Node> queue = new LinkedList<Node>();
        node.visited = true;
        queue.offer(node);

        Node thisNode, toNode;
        while (!queue.isEmpty()) {
            thisNode = queue.poll();
            nodes.add(thisNode);

            for (Edge e : graph.getAdjacent(thisNode)) {
                toNode = e.to;
                if (!toNode.visited) {
                    toNode.visited = true;
                    queue.offer(toNode);
                }
            }
        }
        return nodes;
    }

    /**
     * Returns the connected component for the given {@link Node}
     *
     * @param graph
     * @param node
     * @return
     */
    public static Set<Node> connectedComponent(Graph graph, Node node) {
        Set<Node> connectedComponents = new HashSet<Node>();
        connectedComponents(graph, node, connectedComponents);
        return connectedComponents;
    }

    private static void connectedComponents(Graph graph, Node node, Set<Node> connectedComponents) {
        node.visited = true;
        connectedComponents.add(node);
        Collection<Node> neighbors = graph.getNeighbors(node);
        for (Node neighbor : neighbors) {
            if (!neighbor.visited) {
                connectedComponents(graph, neighbor, connectedComponents);
            }
        }
    }
}
