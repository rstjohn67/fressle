package com.fressle.graph;

public class Edge implements Comparable<Edge> {

    final Node from, to;
    final int weight;

    public Edge(final Node source, final Node dest) {
        from = source;
        to = dest;
        weight = 1;
    }

    public Edge(final Node argFrom, final Node argTo, final int argWeight) {
        from = argFrom;
        to = argTo;
        weight = argWeight;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final Edge other = (Edge) obj;
        if (this.from != other.from && (this.from == null || !this.from.equals(other.from))) {
            return false;
        }
        if (this.to != other.to && (this.to == null || !this.to.equals(other.to))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.from != null ? this.from.hashCode() : 0);
        hash = 41 * hash + (this.to != null ? this.to.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(final Edge argEdge) {
        return argEdge.from.compareTo(to);
    }
    
    @Override
    public String toString() {
        return from.toString() + "->" + to.toString();
    }
}