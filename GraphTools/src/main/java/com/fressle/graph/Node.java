package com.fressle.graph;

public class Node implements Comparable<Node> {

    final String name;
    boolean visited = false;   // used for Kosaraju's algorithm and Edmonds's algorithm
//    String clusterId;
    int lowlink = -1;          // used for Tarjan's algorithm
    int index = -1;            // used for Tarjan's algorithm

    public Node(final String argName) {
        name = argName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Node other = (Node) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    
    @Override
    public int compareTo(final Node argNode) {
        return argNode.name.compareToIgnoreCase(this.name);
    }
    
    
    @Override
    public String toString() {
        return name;
    }
}