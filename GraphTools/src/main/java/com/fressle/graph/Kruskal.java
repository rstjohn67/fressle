package com.fressle.graph;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Kruskal's algorithm finds a minimum spanning tree for a connected, weighted, undirected graph.
 * @author rstjohn
 */
public class Kruskal {

    public ArrayList<Edge> getMST(Node[] nodes, ArrayList<Edge> edges) {
        Collections.sort(edges);
        ArrayList<Edge> MST = new ArrayList<Edge>();
        DisjointSet<Node> nodeset = new DisjointSet<Node>();
        nodeset.createSubsets(nodes);
        for (Edge e : edges) {
            if (nodeset.find(e.from) != nodeset.find(e.to)) {
                nodeset.merge(nodeset.find(e.from), nodeset.find(e.to));
                MST.add(e);
            }
        }
        return MST; //wors
    }
}