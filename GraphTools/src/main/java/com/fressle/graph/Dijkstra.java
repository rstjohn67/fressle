package com.fressle.graph;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Dijkstra's algorithm finds the shortest path from a source node to any/all target nodes.
 * @author rstjohn
 */
public class Dijkstra {

   // assumes Nodes are numbered 0, 1, ... n and that the source Node is 0
   public ArrayList<Node> findShortestPath(Node[] nodes, Edge[] edges, Node target) {
       int[][] weight = initializeWeight(nodes, edges);
       int[] D = new int[nodes.length];
       Node[] P = new Node[nodes.length];
       ArrayList<Node> C = new ArrayList<Node>();

       // initialize:
       // (C)andidate set,
       // (D)yjkstra special path length, and
       // (P)revious Node along shortest path
       for(int i=0; i<nodes.length; i++){
           C.add(nodes[i]);
           D[i] = weight[0][i];
           if(D[i] != Integer.MAX_VALUE){
               P[i] = nodes[0];
           }
       }

       // crawl the graph
       for(int i=0; i<nodes.length; i++){
           // find the lightest Edge among the candidates
           int l = Integer.MAX_VALUE;
           Node n = nodes[0];
           for(Node node : C){
               if(D[node.index] < l){
                   n = node;
                   l = D[node.index];
               }
           }
           C.remove(n);

           // see if any Edges from this Node yield a shorter path than from source->that Node
           for(int j=0; j<nodes.length; j++){
               if(D[n.index] != Integer.MAX_VALUE && weight[n.index][j] != Integer.MAX_VALUE && D[n.index]+weight[n.index][j] < D[j]){
                   // found one, update the path
                   D[j] = D[n.index] + weight[n.index][j];
                   P[j] = n;
               }
           }
       }
       // we have our path. reuse C as the result list
       C.clear();
       int loc = target.index;
       C.add(target);
       // backtrack from the target by P(revious), adding to the result list
       while(P[loc] != nodes[0]){
           if(P[loc] == null){
               // looks like there's no path from source to target
               return null;
           }
           C.add(0, P[loc]);
           loc = P[loc].index;
       }
       C.add(0, nodes[0]);
       return C;
   }

   private int[][] initializeWeight(Node[] nodes, Edge[] edges){
       int[][] weight = new int[nodes.length][nodes.length];
       for(int i=0; i<nodes.length; i++){
           Arrays.fill(weight[i], Integer.MAX_VALUE);
       }
       for(Edge e : edges){
           weight[e.from.index][e.to.index] = e.weight;
       }
       return weight;
   }
}