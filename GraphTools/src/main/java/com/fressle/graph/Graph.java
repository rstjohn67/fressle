package com.fressle.graph;

import com.google.common.collect.TreeMultimap;
import java.util.*;

public class Graph {

    public static final int DIRECTED = 0;
    public static final int UNDIRECTED = 1;
    private TreeMultimap<Node, Edge> adjacencies = TreeMultimap.create();
    private int type;

    public Graph() {
        this(UNDIRECTED);
    }

    public Graph(int type) {
        this.type = type;
    }

    public void addEdge(Node source, Node target) {
        addEdge(source, target, 1);
    }

    public void addEdge(Node source, Node target, int weight) {
        Edge forwardEdge = new Edge(source, target, weight);
        adjacencies.put(source, forwardEdge);

        if (type == UNDIRECTED) {
            Edge backEdge = new Edge(target, source, weight);
            adjacencies.put(target, backEdge);
        }
    }

    public SortedSet<Edge> getAdjacent(Node source) {
        return adjacencies.get(source);
    }

    public void reverseEdge(Edge e) {
        adjacencies.get(e.from).remove(e);
        addEdge(e.to, e.from, e.weight);
    }

    public void reverseGraph() {
        adjacencies = getReversedList().adjacencies;
    }

    public Graph getReversedList() {
        Graph newlist = new Graph();
        for (Edge edge : adjacencies.values()) {
            newlist.addEdge(edge.to, edge.from, edge.weight);
        }
        return newlist;
    }

    public Set<Node> getSourceNodeSet() {
        return adjacencies.keySet();
    }

    public Collection<Node> getNeighbors(Node node) {
        Collection<Edge> edges = getAdjacent(node);
        List<Node> neighbors = new ArrayList<Node>();
        for (Edge edge : edges) {
            neighbors.add(edge.to);
        }
        return neighbors;
    }

    public Collection<Edge> getEdges() {
        List<Edge> edges = new ArrayList<Edge>();
        for (Edge e : adjacencies.values()) {
            edges.add(e);
        }
        return edges;
    }
}