#!/bin/bash

INPUT=/home/rstjohn/giraph/nodelist.txt
OUTPUT=/home/rstjohn/giraph/cc.txt

hadoop fs -rmr /home/rstjohn/giraph/cc.txt

JAR=target/ConnectedComponents.jar

hadoop jar $JAR $INPUT $OUTPUT 2
