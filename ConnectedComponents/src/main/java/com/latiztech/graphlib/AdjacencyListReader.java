package com.latiztech.graphlib;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.giraph.graph.BasicVertex;
import org.apache.giraph.graph.BspUtils;
import org.apache.giraph.lib.TextVertexInputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordReader;

/**
 *
 * @author rstjohn
 */
public class AdjacencyListReader extends TextVertexInputFormat.TextVertexReader<Text, Text, Text, Text> {

    /**
     * Separator of the vertex and neighbors
     */
    private static final Pattern SEPARATOR = Pattern.compile("[\t ]");

    /**
     * Constructor with the line record reader.
     *
     * @param lineRecordReader Will read from this line.
     */
    public AdjacencyListReader(RecordReader<LongWritable, Text> lineRecordReader) {
        super(lineRecordReader);
    }

    @Override
    public BasicVertex<Text, Text, Text, Text> getCurrentVertex() throws IOException, InterruptedException {
        BasicVertex<Text, Text, Text, Text> vertex = BspUtils.<Text, Text, Text, Text>createVertex(getContext().getConfiguration());

        Text line = getRecordReader().getCurrentValue();

        String[] tokens = SEPARATOR.split(line.toString());
        Map<Text, Text> edges = Maps.newHashMapWithExpectedSize(tokens.length - 1);

        Text vertexId = new Text(tokens[0]);
        for (int n = 1; n < tokens.length; n++) {
            edges.put(new Text(tokens[n]), new Text("1"));
        }
        vertex.initialize(vertexId, vertexId, edges, Lists.<Text>newArrayList());

        return vertex;
    }

    @Override
    public boolean nextVertex() throws IOException, InterruptedException {
        return getRecordReader().nextKeyValue();
    }
}
