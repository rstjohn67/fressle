/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.latiztech.graphlib;

import com.google.common.base.Preconditions;
import java.io.IOException;
import java.util.Iterator;
import org.apache.giraph.graph.EdgeListVertex;
import org.apache.giraph.graph.GiraphJob;
import org.apache.giraph.graph.VertexReader;
import org.apache.giraph.graph.VertexWriter;
import org.apache.giraph.lib.TextVertexInputFormat;
import org.apache.giraph.lib.TextVertexOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

/**
 * Implementation of the HCC algorithm that identifies connected components and
 * assigns each vertex its "component identifier" (the smallest vertex id in the
 * component)
 *
 * The idea behind the algorithm is very simple: propagate the smallest vertex
 * id along the edges to all vertices of a connected component. The number of
 * supersteps necessary is equal to the length of the maximum diameter of all
 * components + 1
 *
 * The original Hadoop-based variant of this algorithm was proposed by Kang,
 * Charalampos, Tsourakakis and Faloutsos in "PEGASUS: Mining Peta-Scale
 * Graphs", 2010
 *
 * http://www.cs.cmu.edu/~ukang/papers/PegasusKAIS.pdf
 */
public class ConnectedComponents extends EdgeListVertex<Text, Text, Text, Text> implements Tool {

    /**
     * Configuration
     */
    private Configuration conf;

    @Override
    public void compute(Iterator<Text> msgIterator) {

        String currentComponent = getVertexValue().toString();

        // First superstep is special, because we can simply look at the neighbors
        if (getSuperstep() == 0) {
            for (Iterator<Text> edges = iterator(); edges.hasNext();) {
                String neighbor = edges.next().toString();
                if (neighbor.compareTo(currentComponent) > 0) {
                    currentComponent = neighbor;
                }
            }

            // Only need to send value if it is not the own id
            if (!currentComponent.equals(getVertexValue().toString())) {
                setVertexValue(new Text(currentComponent));
                for (Iterator<Text> edges = iterator(); edges.hasNext();) {
                    String neighbor = edges.next().toString();
                    if (neighbor.compareTo(currentComponent) < 0) {
                        sendMsg(new Text(neighbor), getVertexValue());
                    }
                }
            }

            voteToHalt();
            return;
        }

        boolean changed = false;
        // did we get a smaller id ?
        for (Text message : getMessages()) {
            String candidateComponent = message.toString();
            if (candidateComponent.compareTo(currentComponent) > 0) {
                currentComponent = candidateComponent;
                changed = true;
            }
        }

        // propagate new component id to the neighbors
        if (changed) {
            setVertexValue(new Text(currentComponent));
            sendMsgToAllEdges(getVertexValue());
        }
        voteToHalt();
    }

    /**
     * VertexInputFormat that supports {@link SimpleShortestPathsVertex}
     */
    public static class ConnectedComponentsVertexInputFormat extends TextVertexInputFormat<Text, Text, Text, Text> {

        @Override
        public VertexReader<Text, Text, Text, Text> createVertexReader(InputSplit split, TaskAttemptContext context) throws IOException {
            return new AdjacencyListReader(textInputFormat.createRecordReader(split, context));
        }
    }

    /**
     * VertexOutputFormat that supports {@link SimpleShortestPathsVertex}
     */
    public static class ConnectedComponentsVertexOutputFormat extends TextVertexOutputFormat<Text, Text, Text> {

        @Override
        public VertexWriter<Text, Text, Text> createVertexWriter(TaskAttemptContext context) throws IOException, InterruptedException {
            RecordWriter<Text, Text> recordWriter = textOutputFormat.getRecordWriter(context);
            return new HDFSVertexWriter(recordWriter);
        }
    }

    @Override
    public Configuration getConf() {
        return conf;
    }

    @Override
    public void setConf(Configuration conf) {
        this.conf = conf;
    }

    @Override
    public int run(String[] argArray) throws Exception {
        Preconditions.checkArgument(argArray.length == 3,
                "run: Must have 3 arguments <input path> <output path> <# of workers>");
        conf.set(GiraphJob.ZOOKEEPER_LIST, "localhost:2181");

        GiraphJob job = new GiraphJob(conf, getClass().getName());

        job.setVertexClass(getClass());
        job.setVertexInputFormatClass(ConnectedComponentsVertexInputFormat.class);
        job.setVertexOutputFormatClass(ConnectedComponentsVertexOutputFormat.class);

        FileInputFormat.addInputPath(job.getInternalJob(), new Path(argArray[0]));
        FileOutputFormat.setOutputPath(job.getInternalJob(), new Path(argArray[1]));
        job.setWorkerConfiguration(Integer.parseInt(argArray[2]), Integer.parseInt(argArray[2]), 100.0f);

        return job.run(true) ? 0 : -1;
    }

    /**
     * Can be used for command line execution.
     *
     * @param args Command line arguments.
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new ConnectedComponents(), args));
    }
}
