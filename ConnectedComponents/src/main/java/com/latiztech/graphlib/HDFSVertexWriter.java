package com.latiztech.graphlib;

import java.io.IOException;
import org.apache.giraph.graph.BasicVertex;
import org.apache.giraph.lib.TextVertexOutputFormat;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;

/**
 *
 * @author rstjohn
 */
public class HDFSVertexWriter extends TextVertexOutputFormat.TextVertexWriter<Text, Text, Text> {

    /**
     * VertexWriter that supports {@link SimpleShortestPathsVertex}
     */
    /**
     * Vertex writer with the internal line writer.
     *
     * @param lineRecordWriter Wil actually be written to.
     */
    public HDFSVertexWriter(RecordWriter<Text, Text> lineRecordWriter) {
        super(lineRecordWriter);
    }

    @Override
    public void writeVertex(BasicVertex<Text, Text, Text, ?> vertex) throws IOException, InterruptedException {

        StringBuilder output = new StringBuilder();
        output.append(vertex.getVertexId().toString());
        output.append('\t');
        output.append(vertex.getVertexValue().toString());
        getRecordWriter().write(new Text(output.toString()), null);

//
//            JSONArray jsonVertex = new JSONArray();
//            try {
//                jsonVertex.put(vertex.getVertexId().get());
//                jsonVertex.put(vertex.getVertexValue().get());
//                JSONArray jsonEdgeArray = new JSONArray();
//                for (LongWritable targetVertexId : vertex) {
//                    JSONArray jsonEdge = new JSONArray();
//                    jsonEdge.put(targetVertexId.get());
//                    jsonEdge.put(vertex.getEdgeValue(targetVertexId).get());
//                    jsonEdgeArray.put(jsonEdge);
//                }
//                jsonVertex.put(jsonEdgeArray);
//            } catch (JSONException e) {
//                throw new IllegalArgumentException("writeVertex: Couldn't write vertex " + vertex);
//            }
//            getRecordWriter().write(new Text(jsonVertex.toString()), null);
    }
}
