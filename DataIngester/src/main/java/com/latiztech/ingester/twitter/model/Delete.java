package com.latiztech.ingester.twitter.model;

/**
 *
 * @author rstjohn
 */
public class Delete {

    private Status status;


    public Delete(Status status) {
        this.status = status;
    }


    public Status getStatus() {
        return status;
    }
}
