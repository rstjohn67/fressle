package com.latiztech.ingester.twitter.model;

import java.util.Collection;

public class Geo {

    private String type;
    private Collection<Double> coordinates;

    public Geo(String type, Collection<Double> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public Collection<Double> getCoordinates() {
        return coordinates;
    }
}
