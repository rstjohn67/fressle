package com.latiztech.ingester.twitter.model;

/**
 *
 * @author rstjohn
 *
 */

/*
 *
 * {"user": {"following":null,
 * "profile_background_image_url":"http:\/\/a0.twimg.com\/profile_background_images\/208073369\/twitter-background-rowanberry-n.jpg",
 * "favourites_count":4, "verified":false, "time_zone":"Arizona",
 * "profile_text_color":"000000", "follow_request_sent":null,
 * "profile_sidebar_fill_color":"bdbdbd", "id_str":"51210944",
 * "profile_background_tile":true, "followers_count":1868, "created_at":"Fri Jun
 * 26 20:25:52 +0000 2009", "description":"", "is_translator":false,
 * "show_all_inline_media":false, "geo_enabled":false,
 * "profile_link_color":"b91219", "location":"Arizona", "listed_count":165,
 * "profile_sidebar_border_color":"000000", "protected":false,
 * "profile_image_url":"http:\/\/a2.twimg.com\/profile_images\/1186879734\/twittericon_normal.jpg",
 * "lang":"en", "name":"MaritimeArts on Etsy", "contributors_enabled":false,
 * "statuses_count":29702, "notifications":null,
 * "profile_use_background_image":true, "screen_name":"foxtrotcharlie1",
 * "id":51210944, "utc_offset":-25200, "friends_count":806,
 * "profile_background_color":"ffffff",
 * "url":"http:\/\/www.maritimearts.etsy.com" }, "in_reply_to_screen_name":null,
 * "in_reply_to_status_id_str":null, "in_reply_to_user_id":null,
 * "contributors":null, "coordinates":null, "retweeted":false, "text":"RT
 * @pearlatplay: Leaf Charm Necklace. Pearl. Silver. Gold - Budding Spring
 * http:\/\/etsy.me\/hbBVPg #Etsy #Jewelry &lt;Pretty!",
 * "in_reply_to_user_id_str":null, "retweet_count":0,
 * "in_reply_to_status_id":null, "id_str":"42806051636129792", "source":"\u003Ca
 * href=\"http:\/\/www.hootsuite.com\"
 * rel=\"nofollow\"\u003EHootSuite\u003C\/a\u003E", "created_at":"Wed Mar 02
 * 04:38:52 +0000 2011", "truncated":false, "entities":{
 * "user_mentions":[{"indices":[3,15],"id_str":"50206106","name":"Pearl"
 * ,"screen_name"
 * :"pearlatplay","id":50206106}],"urls":[{"expanded_url":null,"indices"
 * :[75,96],"url":
 * "http:\/\/etsy.me\/hbBVPg"}],"hashtags":[{"indices":[97,102],"text":"Etsy"},{"indices":[103,111],"text":"Jewelry"}]},"geo":null,"place":null,"favorited":false,"id":42806051636129792}
 */
public class Tweet {

    private String source;

    private String text;

    private String created_at;

    private String retweet_count;

    private User user;

    private Entities entities;

    private Delete delete;

    private boolean retweeted;

    private long id;

    private long in_reply_to_user_id;

    private String in_reply_to_user_id_str;

    private Geo geo;


    public Tweet(
            Delete delete,
            String source,
            String text,
            User user,
            Entities entities,
            Urls urls,
            Boolean retweeted,
            long id,
            String created_at,
            String retweet_count,
            String in_reply_to_user_id_str,
            long in_reply_to_user_id, Geo geo) {
        this.delete = delete;
        this.source = source;
        this.text = text;
        this.user = user;
        this.entities = entities;
        this.retweeted = retweeted;
        this.id = id;
        this.geo = geo;
        this.created_at = created_at;
        this.retweet_count = retweet_count;
        this.in_reply_to_user_id = in_reply_to_user_id;
        this.in_reply_to_user_id_str = in_reply_to_user_id_str;
    }


    public Geo getGeo() {
        return geo;
    }


    public String getSource() {
        return source;
    }


    public String getText() {
        return text;
    }


    public User getUser() {
        return user;
    }


    public Entities getEntities() {
        return entities;
    }


    public boolean isRetweeted() {
        return retweeted;
    }


    public long getId() {
        return id;
    }


    public String getCreated_at() {
        return created_at;
    }


    public String getRetweet_count() {
        return retweet_count;
    }


    public long getIn_reply_to_user_id() {
        return in_reply_to_user_id;
    }


    public String getIn_reply_to_user_id_str() {
        return in_reply_to_user_id_str;
    }


    public Delete getDelete() {
        return delete;
    }
}
