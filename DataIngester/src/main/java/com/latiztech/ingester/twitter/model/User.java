package com.latiztech.ingester.twitter.model;

public class User {

    private int followers_count, id, listed_count, utc_offset, friends_count, statuses_count;
    private String screen_name, name, url, time_zone, created_at, profile_image_url;

    public User(
            String screen_name, 
            String name,
            String url, 
            String time_zone, 
            String created_at, 
            String profile_image_url,
            int followers_count, 
            int id, 
            int listed_count, 
            int utc_offset, 
            int friends_count,
            int statuses_count) {
        this.screen_name = screen_name;
        this.url = url;
        this.time_zone = time_zone;
        this.created_at = created_at;
        this.profile_image_url = profile_image_url;
        this.followers_count = followers_count;
        this.id = id;
        this.listed_count = listed_count;
        this.utc_offset = utc_offset;
        this.friends_count = friends_count;
        this.statuses_count = statuses_count;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public int getId() {
        return id;
    }

    public int getListed_count() {
        return listed_count;
    }

    public int getUtc_offset() {
        return utc_offset;
    }

    public int getFriends_count() {
        return friends_count;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public String getUrl() {
        return url;
    }

    public String getTime_zone() {
        return time_zone;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getProfile_image_url() {
        return profile_image_url;
    }

    public int getStatuses_count() {
        return statuses_count;
    }

    public String getName() {
        return name;
    }
    
}
