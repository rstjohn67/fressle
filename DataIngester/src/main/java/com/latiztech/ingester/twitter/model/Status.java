package com.latiztech.ingester.twitter.model;

/**
 *
 * @author rstjohn
 */
public class Status {

    private String user_id_str, id_str;

    private long user_id, id;


    public Status(String user_id_str, String id_str, long user_id, long id) {
        this.user_id_str = user_id_str;
        this.id_str = id_str;
        this.user_id = user_id;
        this.id = id;
    }


    public long getId() {
        return id;
    }


    public String getId_str() {
        return id_str;
    }


    public long getUser_id() {
        return user_id;
    }


    public String getUser_id_str() {
        return user_id_str;
    }
}
