package com.latiztech.ingester.twitter.model;

public class Urls {

    private String url;

    public Urls(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}