package com.latiztech.ingester.twitter.model;

public class HashTags {

    private String text;

    public HashTags(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
