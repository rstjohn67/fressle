package com.latiztech.ingester.twitter.model;

import java.util.Collection;

public class Entities {

    private Collection<UserMentions> user_mentions;
    private Collection<Urls> urls;
    private Collection<HashTags> hashtags;

    public Entities(
            Collection<UserMentions> user_mentions, 
            Collection<Urls> urls, 
            Collection<HashTags> hashtags
            ) {
        this.user_mentions = user_mentions;
        this.urls = urls;
        this.hashtags = hashtags;
    }

    public Collection<UserMentions> getUser_mentions() {
        return user_mentions;
    }

    public Collection<Urls> getUrls() {
        return urls;
    }

    public Collection<HashTags> getHashtags() {
        return hashtags;
    }
}
