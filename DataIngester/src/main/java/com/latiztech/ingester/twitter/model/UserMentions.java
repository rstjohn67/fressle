package com.latiztech.ingester.twitter.model;

public class UserMentions {
    private String name, screen_name;
    private int id;

    public UserMentions(String name, String screen_name, int id) {
        super();
        this.name = name;
        this.screen_name = screen_name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public int getId() {
        return id;
    }

}
