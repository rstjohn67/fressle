package com.latiztech.ingester.sinks;

import com.latiztech.ingester.events.ConsumerEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.hadoop.io.Text;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author rstjohn
 */
public class HadoopSink extends Sink {


    private int count = 0;
    private final Configuration conf;
    private Writer writer;


    public HadoopSink(Configuration conf) throws IOException {
        this.conf = conf;
        this.writer = createSequenceFileWriter();
    }

    private Writer createSequenceFileWriter() throws IOException {
        if(writer != null) {
            writer.close();
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        Path path = new Path(conf.get("hdfs.output.file.base") + "." + sdf.format(new Date()) + ".seq");
        FileSystem fs = FileSystem.get(conf);
        return SequenceFile.createWriter(fs, conf, path, LongWritable.class, Text.class);
    }

    @Override
    public synchronized void consume(ConsumerEvent event) {

        JSONParser parser = new JSONParser();
        JSONObject root;

        try {
            root = (JSONObject) parser.parse(event.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(HadoopSink.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

        if (root.get("delete") != null) {
            System.out.println("Skipping deleted tweet");
            return;
        }

        if (count == 500) {
            count = 0;
            try {
                writer = createSequenceFileWriter();
            } catch (IOException ex) {
                Logger.getLogger(HadoopSink.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        try {
            writer.append(new LongWritable(event.getDate().getTime()), new Text(event.getMessage()));
            count++;
        } catch (IOException ex) {
            Logger.getLogger(HadoopSink.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public void closed() {
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(HadoopSink.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
