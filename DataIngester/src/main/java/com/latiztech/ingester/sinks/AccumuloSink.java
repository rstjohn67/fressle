package com.latiztech.ingester.sinks;

import com.google.gson.Gson;
import com.latiztech.ingester.events.ConsumerEvent;
import com.latiztech.ingester.twitter.model.Tweet;
import com.latiztech.ingester.twitter.model.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.accumulo.core.client.*;
import org.apache.accumulo.core.client.admin.TableOperations;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;

/**
 *
 * @author rstjohn
 */
public class AccumuloSink extends Sink {

    private static final int CACHE_SIZE = 500;

    private final Configuration conf;

    private Gson gson;

    private BatchWriter writer;

    private String separator = "\u0000";
    
    private List<Tweet> items;


    public AccumuloSink(Configuration conf) {
        this.conf = conf;
        initialize();
    }


    private void initialize() {
        items = new ArrayList<Tweet>();
        gson = new Gson();

        String instanceName = "cloudbase";
        String zooKeepers = "127.0.0.1:2181";
        String user = "root";
        String pass = "secret";
        String table = "twitter_facts";

        try {
            ZooKeeperInstance instance = new ZooKeeperInstance(instanceName, zooKeepers);
            Connector connector = instance.getConnector(user, pass.getBytes());
            TableOperations tableOperations = connector.tableOperations();
            if (!tableOperations.exists(table)) {
                tableOperations.create(table);
            }
            writer = connector.createBatchWriter(table, 5000000L, 300L, 20);
        } catch (Exception ex) {
            Logger.getLogger(AccumuloSink.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public synchronized void consume(ConsumerEvent event) {
        Tweet tweet = gson.fromJson(event.getMessage(), Tweet.class);

        if (tweet == null || tweet.getDelete() != null) {
            System.out.println("Skipping: " + event.getMessage());
            return;
        }

        User user = tweet.getUser();
        if (user == null) {
            System.out.println("Null User: " + event.getMessage());
            return;
        }

        if (items.size() > CACHE_SIZE) {
            persistTweets();
        }

        System.out.println(tweet.getUser().getScreen_name());
    }


    private void persistTweets() {
        if (items.isEmpty()) {
            return;
        }

        long now = new Date().getTime();
        Mutation mutation = new Mutation(new Text(String.valueOf(now)));
        UUID uuid = UUID.randomUUID();
        for (Tweet tweet : items) {
            appendFieldIndex(uuid.toString(), mutation, tweet);
            appendEventData(uuid.toString(), mutation, tweet);
        }
    }


    private void appendFieldIndex(String uuid, Mutation mutation, Tweet tweet) {
        Text cf = new Text("fi" + separator + "screen_name");
        Text cq = new Text(tweet.getUser().getScreen_name() + separator + "twitter" + separator + uuid);
        Value v = new Value("".getBytes());
        mutation.put(cf, cq, v);
    }


    private void appendEventData(String uuid, Mutation mutation, Tweet tweet) {
        Text cf = new Text("twitter" + separator + uuid);
        Text cq = new Text("user_name" + separator + tweet.getUser().getScreen_name());
        Value v = new Value("".getBytes());
        mutation.put(cf, cq, v);
    }


    @Override
    public void closed() {
    }
}
