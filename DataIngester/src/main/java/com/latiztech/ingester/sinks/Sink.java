package com.latiztech.ingester.sinks;

import com.latiztech.ingester.events.ConsumerEvent;

/**
 *
 * @author rstjohn
 */
public abstract class Sink {
    public abstract void consume(ConsumerEvent event);
    public abstract void closed();
}
