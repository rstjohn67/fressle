package com.latiztech.ingester;

import com.latiztech.ingester.events.ProducerEvent;
import com.latiztech.ingester.events.ProducerListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rstjohn
 */
public class TupleProducer extends Thread {

    private BufferedReader reader;

    private BlockingQueue<String> queue;

    private List<ProducerListener> producerListeners;


    public TupleProducer(BlockingQueue<String> queue) throws MalformedURLException, IOException {
        Authenticator.setDefault(new WebAuthenticator());
        this.queue = queue;
        initialize();
    }


    private void initialize() throws MalformedURLException, IOException {
        URL url = new URL(Props.getInstance().getURL());
        URLConnection connection = url.openConnection();
        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        producerListeners = new ArrayList<ProducerListener>();
    }


    public void addProducerListener(ProducerListener listener) {
        int index = producerListeners.indexOf(listener);
        if (index == -1) {
            producerListeners.add(listener);
        }
    }


    public ProducerListener removeProducerListener(ProducerListener listener) {
        int index = producerListeners.indexOf(listener);
        if (index != -1) {
            return producerListeners.remove(index);
        }
        return null;
    }


    public List<ProducerListener> getProducerListeners() {
        return producerListeners;
    }


    private synchronized void emit(String line) {
        for(ProducerListener listeners : producerListeners) {
            listeners.onProduced(new ProducerEvent(line, new Date()));
        }
    }


    @Override
    public void run() {
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                queue.add(line);
                emit(line);
            }
        } catch (Exception ex) {
            Logger.getLogger(TupleProducer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
