package com.latiztech.ingester.events;

/**
 *
 * @author rstjohn
 */
public interface ProducerListener {
    public void onProduced(ProducerEvent event);
}
