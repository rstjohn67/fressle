package com.latiztech.ingester.events;

/**
 *
 * @author rstjohn
 */
public interface TimeoutListener {
    public void onTimeout(TimeoutEvent event);
}
