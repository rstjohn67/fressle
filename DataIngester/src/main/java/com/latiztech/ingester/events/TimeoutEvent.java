package com.latiztech.ingester.events;

import java.util.Date;

/**
 *
 * @author rstjohn
 */
public class TimeoutEvent {

    private String message;

    private Date date;


    public TimeoutEvent(String message, Date date) {
        this.message = message;
        this.date = date;
    }


    public Date getDate() {
        return date;
    }


    public void setDate(Date date) {
        this.date = date;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }
}
