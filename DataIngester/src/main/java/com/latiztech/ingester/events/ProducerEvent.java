package com.latiztech.ingester.events;

import java.util.Date;

/**
 *
 * @author rstjohn
 */
public class ProducerEvent {

    private String message;
    private Date date;

    public ProducerEvent(String message, Date date) {
        this.message = message;
        this.date = date;
    }

    /**
     * Get the value of date
     *
     * @return the value of date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set the value of date
     *
     * @param date new value of date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get the value of message
     *
     * @return the value of message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set the value of message
     *
     * @param message new value of message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
