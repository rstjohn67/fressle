package com.latiztech.ingester.events;

/**
 *
 * @author rstjohn
 */
public interface ConsumerListener {
    public void onConsumed(ConsumerEvent event);
}
