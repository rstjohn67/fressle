package com.latiztech.ingester;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 *
 * @author rstjohn
 */
public class WebAuthenticator extends Authenticator {

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        String username = Props.getInstance().getUsername();
        String password = Props.getInstance().getPassword();
        
        return new PasswordAuthentication(username, password.toCharArray());
    }
}
