package com.latiztech.ingester;

import com.latiztech.ingester.events.ConsumerEvent;
import com.latiztech.ingester.events.ConsumerListener;
import com.latiztech.ingester.events.TimeoutEvent;
import com.latiztech.ingester.events.TimeoutListener;
import com.latiztech.ingester.sinks.AccumuloSink;
import com.latiztech.ingester.sinks.HadoopSink;
import com.latiztech.ingester.sinks.Sink;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Main extends Configured implements Tool {

    public static final int MAX_RETRIES = 60 * 24;

    public static final int SECONDS_TO_WAIT = 60;

    private boolean timeout = false;

    private List<Sink> sinks;


    public Main() throws IOException {
    }


    @Override
    public int run(String[] strings) throws Exception {
        Configuration conf = getConf();
        sinks = new ArrayList<Sink>();
        sinks.add(new HadoopSink(conf));
        sinks.add(new AccumuloSink(conf));

        BlockingQueue<String> queue = new LinkedBlockingQueue<String>();

        // Initialize producer thread
        TupleProducer producer = new TupleProducer(queue);

        // Initialize consumer thread
        ConsumerEventListener consumerListener = new ConsumerEventListener();
        TupleConsumer consumer = new TupleConsumer(queue);
        consumer.addConsumerListener(consumerListener);
        consumer.addTimeoutListener(consumerListener);

        // Start threads.
        producer.start();
        consumer.start();

        // Add monitoring.  Try to restart producer if the feed is lost 
        int retries = 0;

        while (true) {

            if (retries > MAX_RETRIES) {
                break;
            }

            if (timeout) {
                System.out.println("Driver restarting twitter feed at: " + new Date());

                producer.interrupt();
                producer = new TupleProducer(queue);
                producer.start();

                timeout = false;
                retries++;
            } else {
                retries = 0;
            }

            try {
                Thread.sleep(SECONDS_TO_WAIT * 1000);
            } catch (InterruptedException ex) {
            }
        }

        notifyProgramStopped();
        return 0;
    }


    private void notifyProgramStopped() {
        for (Sink sink : sinks) {
            sink.closed();
        }
        try {
            EmailNotification.ingestorStopped(new TimeoutEvent("Ingester program was stopped.", new Date()));
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("hdfs.output.file.base", "/home/rstjohn/twitter");
        int res = ToolRunner.run(conf, new Main(), args);
        System.exit(res);
    }

    /**
     *
     */
    private class ConsumerEventListener implements ConsumerListener, TimeoutListener {

        @Override
        public void onConsumed(ConsumerEvent event) {
            for (Sink sink : sinks) {
                sink.consume(event);
            }
        }


        @Override
        public void onTimeout(TimeoutEvent event) {
            try {
                EmailNotification.ingestorStopped(new TimeoutEvent("Ingester producer was restarted.", new Date()));
                timeout = true;
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
