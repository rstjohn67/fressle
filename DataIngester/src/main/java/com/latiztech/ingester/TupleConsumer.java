package com.latiztech.ingester;

import com.latiztech.ingester.events.ConsumerEvent;
import com.latiztech.ingester.events.ConsumerListener;
import com.latiztech.ingester.events.TimeoutEvent;
import com.latiztech.ingester.events.TimeoutListener;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rstjohn
 */
public class TupleConsumer extends Thread {

    public static final int MAX_WAITED = 10;

    public static final int SECONDS_TO_WAIT = 10;

    private List<ConsumerListener> consumerListeners;

    private List<TimeoutListener> timeoutListeners;

    private BlockingQueue<String> queue;

    private int waited;


    public TupleConsumer(BlockingQueue<String> queue) throws IOException {
        this.queue = queue;
        initialize();
    }


    private void initialize() throws IOException {
        consumerListeners = new ArrayList<ConsumerListener>();
        timeoutListeners = new ArrayList<TimeoutListener>();
    }


    private void cleanup() {
        waited = 0;
    }


    public void addConsumerListener(ConsumerListener listener) {
        int index = consumerListeners.indexOf(listener);
        if (index == -1) {
            consumerListeners.add(listener);
        }
    }


    public void removeConsumerListener(ConsumerListener listener) {
        int index = consumerListeners.indexOf(listener);
        if (index == -1) {
            consumerListeners.add(listener);
        }
    }


    public void addTimeoutListener(TimeoutListener listener) {
        int index = timeoutListeners.indexOf(listener);
        if (index == -1) {
            timeoutListeners.add(listener);
        }
    }


    public void removeTimeoutListener(TimeoutListener listener) {
        int index = consumerListeners.indexOf(listener);
        if (index == -1) {
            timeoutListeners.add(listener);
        }
    }


    private void onConsumed(String item) {
        for (ConsumerListener listener : consumerListeners) {
            listener.onConsumed(new ConsumerEvent(item, new Date()));
        }
    }


    private void onTimeout() {
        double count = MAX_WAITED * (MAX_WAITED + 1) / 2.0;
        String message = "Timeout occurred after waiting " + Math.round(count) + "seconds";
        for (TimeoutListener listener : timeoutListeners) {
            listener.onTimeout(new TimeoutEvent(message, new Date()));
        }
    }


    @Override
    public void run() {

        waited = 0;
        String line;

        while (true) {

            // Consume items from the queue
            while ((line = queue.poll()) != null) {

                // Reset waited, signifying we are able to work off the queue
                waited = 0;

                // Notify a line was polled.
                onConsumed(line);
            }

            // The queue is empty. 
            // Wait increasingly longer periods before stopping the consumer.
            try {
                Thread.sleep(SECONDS_TO_WAIT * 1000 * waited);
            } catch (InterruptedException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }

            if (waited++ > MAX_WAITED) {
                onTimeout();
                break;
            }
        }

        // Cleanup tasks
        cleanup();
    }
}
