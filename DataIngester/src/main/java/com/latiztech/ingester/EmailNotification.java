package com.latiztech.ingester;

import com.latiztech.ingester.events.TimeoutEvent;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author rstjohn
 */
public class EmailNotification {

    public static void ingestorStopped(TimeoutEvent event) throws AddressException, MessagingException {

        // Recipient's email ID needs to be mentioned.
        InternetAddress[] toAddresses = new InternetAddress[2];
        toAddresses[0] = new InternetAddress("rstjohn67@gmail.com");

        // Sender's email ID needs to be mentioned
        String from = "rstjohn67@gmail.com";

        // Assuming you are sending email from localhost
        String host = "smtp.gmail.com";
        int port = 587;

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", host);

        // Get the default Session object.
        final String username = "latiztech@gmail.com";
        final String password = "*FdUX392Z*mXfM!k9&Du";
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {

                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        // Create a default MimeMessage object.
        MimeMessage message = new MimeMessage(session);

        // Set From: header field of the header.
        message.setFrom(new InternetAddress(from));

        // Set To: header field of the header.
        message.addRecipients(Message.RecipientType.TO, toAddresses);

        // Set Subject: header field
        message.setSubject("[Twitter] " + event.getMessage());

        // Now set the actual message
        message.setText("An issue with twitter ingest has occurred at " + event.getDate());

        // Send message
        Transport.send(message);

        System.out.println("Sent message successfully....");
    }
}
