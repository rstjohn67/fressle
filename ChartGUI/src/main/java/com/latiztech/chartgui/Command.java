package com.latiztech.chartgui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * @author Dr. Richard St. John
 * @version $Revision$, $Date$
 */
public class Command {

    public static synchronized boolean killProcess(final String command) {
        Pattern pattern = Pattern.compile(".*" + command + ".*");
        String[] results = Command.executeCommand("ps aux", false);
        String pid;
        StringTokenizer tokenizer;
        for (String s : results) {
//            if (s.contains(command)) {
            if (pattern.matcher(s).matches()) {
                tokenizer = new StringTokenizer(s, " ");
                tokenizer.nextToken().trim();
                pid = tokenizer.nextToken().trim();
                Command.executeCommand("kill -9 " + pid, false);
                return true;
            }
        }
        return false;
    }
    
    public static synchronized String[] executeCommand(final String command, final boolean printToConsole) {
        Runtime rt = Runtime.getRuntime();
        ArrayList<String> sa = new ArrayList<String>();
        try {
            Process process = rt.exec(command);
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                sa.add(line);
            }
            input.close();
            process.waitFor();
            process.getErrorStream().close();
            process.getInputStream().close();
            process.getOutputStream().close();

            if (printToConsole) {
                System.out.println("$ " + command);
                for (int i = 0; i < sa.size(); i++) {
                    System.out.println(sa.get(i));
                }
                System.out.println("");
            }
            return sa.toArray(new String[sa.size()]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
