package com.latiztech.tweetslurper;

import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.internal.json.StatusJSONImpl;

/**
 *
 * @author rstjohn
 */
public class TweetSlurper {

    public static void main(String[] args) throws TwitterException {
        ConfigurationBuilder conf = new ConfigurationBuilder();
        conf.setUser("rstjohn67");
        conf.setPassword("5738twitter");
        
        TwitterStream twitterStream = new TwitterStreamFactory(conf.build()).getInstance();
        twitterStream.setOAuthConsumer("", "");
        twitterStream.addListener(new StatusListener() {
            @Override
            public void onStatus(Status status) {
                System.out.println(((StatusJSONImpl)status).getJson());
            }


            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
            }


            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
            }


            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
            }


            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        });

        twitterStream.sample();
    }
}
