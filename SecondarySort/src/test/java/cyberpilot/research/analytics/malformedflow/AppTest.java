package cyberpilot.research.analytics.malformedflow;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }


    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }


    /**
     * Rigourous Test :-)
     */
    public void testApp() {
//        MalformedFlowMapper mapper = new MalformedFlowMapper();
//        mapper.init();
//        Map<String, String> results;
//        try {
//            results = mapper.parseLine("2012-09-24 15:39:20.669     0.000 TCP       192.168.2.58:51399 ->   173.194.76.125:5222         6      577     1");
//        } catch (ParseException ex) {
//            Logger.getLogger(AppTest.class.getName()).log(Level.SEVERE, null, ex);
//            return;
//        }
//        assertEquals("5222", results.get("port_dst"));
    }
}
