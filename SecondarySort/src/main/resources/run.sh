#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Usage: `basename $0` {walkLength}"
  exit 65
fi

DIR=`pwd $0`

hadoop jar $DIR/MalformedFlow.jar -DwalkLength=$1
