package cyberpilot.research.tools.secondarysort;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 *
 * @author rstjohn
 */
public class SecondarySort {

    public enum Direction {

        FORWARD, REVERSE

    }


    public static void init(Job job) {
        job.setMapOutputKeyClass(SecondarySort.TextPairWritable.class);
        job.setPartitionerClass(SecondarySort.SecondarySortPartitioner.class);
        job.setGroupingComparatorClass(SecondarySort.SecondarySortGroupingComparator.class);
    }

    public static class TextPairWritable extends SecondarySort.TextPair implements WritableComparable<SecondarySort.TextPairWritable> {

        public TextPairWritable() {
            super();
        }


        public TextPairWritable(Text primary, Text secondary) {
            super(primary, secondary);
        }


        public int compareTo(SecondarySort.TextPairWritable t) {
            return super.compareTo(t);
        }

        // Register the comparator.
        static {
            WritableComparator.define(SecondarySort.TextPairWritable.class, new SecondarySort.TextPairWritable.Comparator());
        }
        
        static class Comparator extends WritableComparator {

            public Comparator() {
                super(SecondarySort.TextPairWritable.class);
            }


            @Override
            public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
                int p1l1 = readInt(b1, s1 + SecondarySort.TextPair.BYTES_FIRST_LENGTH_OFFSET);
                int p1l2 = readInt(b1, s1 + SecondarySort.TextPair.BYTES_SECOND_LENGTH_OFFSET);
                
                int p2l1 = readInt(b2, s2 + SecondarySort.TextPair.BYTES_FIRST_LENGTH_OFFSET);
                int p2l2 = readInt(b2, s2 + SecondarySort.TextPair.BYTES_SECOND_LENGTH_OFFSET);
                
                return super.compare(
                        b1, s1 + SecondarySort.TextPair.BYTES_TEXT_START_OFFSET, p1l1+p1l2, 
                        b2, s2 + SecondarySort.TextPair.BYTES_TEXT_START_OFFSET, p2l1+p2l2);
            }
        }
    }

    public static class TextPair implements Writable {

        public static final int BYTES_FIRST_LENGTH_OFFSET = 0;

        public static final int BYTES_SECOND_LENGTH_OFFSET = 4;

        public static final int BYTES_TEXT_START_OFFSET = 8;

        private byte[] first;

        private byte[] second;

        private int length1;

        private int length2;


        public TextPair() {
            length1 = 0;
            length2 = 0;
            first = new byte[length1];
            second = new byte[length2];
        }


        public TextPair(SecondarySort.TextPair textpair) {
            length1 = textpair.first.length;
            first = new byte[length1];
            System.arraycopy(textpair.first, 0, first, 0, length1);

            length2 = textpair.second.length;
            second = new byte[length2];
            System.arraycopy(textpair.second, 0, second, 0, length2);
        }


        public TextPair(Text t1, Text t2) {
            length1 = t1.getLength();
            first = new byte[length1];
            System.arraycopy(t1.getBytes(), 0, first, 0, length1);

            length2 = t2.getLength();
            second = new byte[length2];
            System.arraycopy(t2.getBytes(), 0, second, 0, length2);
        }


        public Text getPrimaryKey() {
            return new Text(first);
        }


        public Text getSecondaryKey() {
            return new Text(second);
        }

        // Serialize as such: length of primary key (4 bytes), length of secondary key (4 bytes)

        public void write(DataOutput d) throws IOException {
            d.writeInt(first.length);
            d.writeInt(second.length);
            d.write(first);
            d.write(second);
        }


        public void readFields(DataInput di) throws IOException {
            length1 = di.readInt();
            length2 = di.readInt();

            first = new byte[length1];
            second = new byte[length2];

            di.readFully(first);
            di.readFully(second);
        }


        public int compareTo(SecondarySort.TextPair tp) {
            int i = WritableComparator.compareBytes(first, 0, first.length, tp.first, 0, tp.first.length);
            if (i == 0) {
                i = WritableComparator.compareBytes(second, 0, second.length, tp.second, 0, tp.second.length);
            }
            return i;
        }


        @Override
        public String toString() {
            return new String(first) + "-" + new String(second);
        }
    }

    public static class SecondarySortGroupingComparator implements RawComparator<SecondarySort.TextPair> {

        public int compare(byte[] b1, int s1, int w1, byte[] b2, int s2, int w2) {
            int offset_text = SecondarySort.TextPair.BYTES_TEXT_START_OFFSET;
            int size1 = WritableComparator.readInt(b1, s1 + SecondarySort.TextPair.BYTES_FIRST_LENGTH_OFFSET);
            int size2 = WritableComparator.readInt(b2, s2 + SecondarySort.TextPair.BYTES_FIRST_LENGTH_OFFSET);
            return WritableComparator.compareBytes(b1, s1 + offset_text, size1, b2, s2 + offset_text, size2);
        }


        public int compare(SecondarySort.TextPair t, SecondarySort.TextPair t1) {
            return t.getPrimaryKey().compareTo(t1.getPrimaryKey());
        }
    }

    /**
     *
     * @param <V>
     */
    public static class SecondarySortPartitioner<V> extends Partitioner<SecondarySort.TextPair, V> {

        @Override
        public int getPartition(SecondarySort.TextPair key, V value, int numPartitions) {
            return Math.abs(key.getPrimaryKey().hashCode()) % numPartitions;
        }
    }
}
