package com.exit4b.camel.sample;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Created with IntelliJ IDEA.
 * User: rstjohn
 * Date: 2/20/13
 * Time: 7:41 PM
 */
public class MyProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String upper = exchange.getIn().getBody().toString().toLowerCase();
        exchange.getOut().setBody(upper);
    }
}
